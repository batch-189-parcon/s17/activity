/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function enterFullName(){
	let fullName = prompt('Enter your fullname:')
	console.log('Hello, ' + fullName)
};

enterFullName();

function enterage(){
	let age = prompt('Enter your age:')
	console.log('You are ' + age + ' years old.')
};

enterage();

function enterLocation(){
	let location = prompt('Enter your location:')
	console.log('You live in ' + location)
};

enterLocation();

function feedBack(){
	alert('Thank you for submitting!')
};

feedBack();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function band1(){
	console.log('1. Panic At The Disco')
};
band1();

function band2(){
	console.log('2. My Chemical Romance')
};
band2();

function band3(){
	console.log('3. Bruno Mars')
};
band3();

function band4(){
	console.log('4. The Weekend')
};
band4();

function band5(){
	console.log('5. December Avenue')
};
band5();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function movie1(){

	console.log('1. Spider-Man (Tobey Maguire)')

	function rotten1(){
		console.log('Rotten Tomatoes Rating: 90%')
	}

	rotten1()
};

movie1();

function movie2(){

	console.log('2. Transformers 1')

	function rotten2(){
		console.log('Rotten Tomatoes Rating: 58%')
	}

	rotten2()
};

movie2();

function movie3(){

	console.log('3. 2 Fast 2 Furious')

	function rotten3(){
		console.log('Rotten Tomatoes Rating: 36%')
	}

	rotten3()
};

movie3();

function movie4(){

	console.log('4. Avengers: Endgame')

	function rotten4(){
		console.log('Rotten Tomatoes Rating: 94%')
	}

	rotten4()
};

movie4();

function movie5(){

	console.log('5. Batman: The Dark Knight')

	function rotten5(){
		console.log('Rotten Tomatoes Rating: 94%')
	}

	rotten5()
};

movie5();	

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/




let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends()